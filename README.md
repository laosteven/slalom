# Slalom Scoring Manager #
-------

Build an **iPad application** meant for managing performance score of every slalom athlete. The main objective is to build an intuitive interface which allows the user to:

* Add an athlete;
* Start and stop the **chronometer**;
* Add a maximum of *3 penalties* for each athlete;
* Set an athlete's scoring to **DNF**; Did Not Finish.

Choosing a Split View Controller for our project, the **Athlete** pane located on the left contains a list of all added / registered slalom athletes with their `name`, `number` and `country` displayed.

The **Detail** pane located on the right side contains three mandatory sections  to display for the judges:

* A list of the `next participants` who will be racing on the slopes;
* A `ranked` list of the athletes;
* A chronometer with specific buttons:
	* DNF
	* Start / Stop
	* Penalties

# Screenshots #
-------

![Capture d’écran 2016-05-06 à 6.58.17 PM.png](https://bitbucket.org/repo/jarKeo/images/1645666353-Capture%20d%E2%80%99%C3%A9cran%202016-05-06%20%C3%A0%206.58.17%20PM.png)

![Capture d’écran 2016-05-06 à 5.38.39 PM.png](https://bitbucket.org/repo/jarKeo/images/1835042142-Capture%20d%E2%80%99%C3%A9cran%202016-05-06%20%C3%A0%205.38.39%20PM.png)

![Capture d’écran 2016-05-06 à 6.58.58 PM.png](https://bitbucket.org/repo/jarKeo/images/1089824091-Capture%20d%E2%80%99%C3%A9cran%202016-05-06%20%C3%A0%206.58.58%20PM.png)

# Objectives #
-------
* Develop in **Objective-C** under **xCode**.
* Familiarize with Apple's design styles such as the dynamic iOS7's Blur effect.
* Learn the necessary steps to build an iOS application.

# For more information #
-------
Visit the following website: [**User interface conception and evaluation** (GTI350)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=gti350) [*fr*]